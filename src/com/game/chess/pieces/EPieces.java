package com.game.chess.pieces;

public enum EPieces {
	Queen(0), King(1), Rook(2), Knight(3), Bishop(4);

	private int value;
	
	private EPieces(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
}