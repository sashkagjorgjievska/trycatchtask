package com.game.chess.pieces;

import java.util.List;

import com.game.chess.Board;
import com.game.chess.Square;
import com.game.chess.utils.ChessUtils;
/**
 * @author sashka gjorgjievska
 * @version 1.0
 *
 */
public class Bishop implements Piece {

	@Override
	public boolean place(Board board, Square square) {
		int row = square.getRow();
		int col = square.getCol();
		int numBoardCols = board.getNumOfCols();
		List<Square> squares = board.getSquares();
		for (Square s : squares) {
			int sRow = s.getRow();
			int sCol = s.getCol();
			if ((sRow + sCol == row + col) || (sRow - sCol == row - col)) {
				if (!ChessUtils.checkAndSetSquare(s)) {
					return false;
				}
			}
		}
		Square squareWithBishop = squares.get(row * numBoardCols + col);
		squareWithBishop.setProtected(false);
		squareWithBishop.setPiece(this);
		return true;

	}

}
