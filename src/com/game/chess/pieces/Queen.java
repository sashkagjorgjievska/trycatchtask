package com.game.chess.pieces;

import java.util.List;

import com.game.chess.Board;
import com.game.chess.Square;
import com.game.chess.utils.ChessUtils;

/**
 * @author sashka gjorgjievska
 * @version 1.0
 *
 */
public class Queen implements Piece {

	@Override
	public boolean place(Board board, Square square) {
		int row = square.getRow();
		int col = square.getCol();
		int numBoardCols = board.getNumOfCols();
		List<Square> squares = board.getSquares();
		for (Square s : squares) {
			int sRow = s.getRow();
			int sCol = s.getCol();
			if (sRow == row || sCol == col || (sRow + sCol == row + col) || (sRow - sCol == row - col)) {
				if (!ChessUtils.checkAndSetSquare(s)) {
					return false;
				}
			}
		}
		Square squareWithQueen = squares.get(row * numBoardCols + col);
		squareWithQueen.setProtected(false);
		squareWithQueen.setPiece(this);
		return true;
	}
	
}
