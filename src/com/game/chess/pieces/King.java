package com.game.chess.pieces;

import java.util.List;

import com.game.chess.Board;
import com.game.chess.Square;
import com.game.chess.utils.ChessUtils;

/**
 * @author sashka gjorgjievska
 * @version 1.0
 *
 */
public class King implements Piece {

	@Override
	public boolean place(Board board, Square square) {
		int row = square.getRow();
		int col = square.getCol();
		List<Square> squares = board.getSquares();
		for (Square s : squares) {
			if (Math.abs(s.getCol() - col) <= 1 && Math.abs(s.getRow() - row) <=1) {
				if (!ChessUtils.checkAndSetSquare(s)) {
					return false;
				}
			}
		}
		Square squareWithKing = board.getSquares().get(row * board.getNumOfCols() + col);
		squareWithKing.setProtected(false);
		squareWithKing.setPiece(this);
		return true;
	}
	
}
