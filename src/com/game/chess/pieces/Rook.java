package com.game.chess.pieces;

import java.util.List;

import com.game.chess.Board;
import com.game.chess.Square;
import com.game.chess.utils.ChessUtils;

/**
 * @author sashka gjorgjievska
 * @version 1.0
 *
 */
public class Rook implements Piece {

	@Override
	public boolean place(Board board, Square square) {
		int row = square.getRow();
		int col = square.getCol();
		List<Square> squares = board.getSquares();
		for (Square s : squares) {
			if (s.getRow() == row || s.getCol() == col) {
				if (!ChessUtils.checkAndSetSquare(s)) {
					return false;
				}
			}
		}
		Square squareWithRook = board.getSquares().get(row * board.getNumOfCols() + col);
		squareWithRook.setProtected(false);
		squareWithRook.setPiece(this);
		return true;
	}
	
}
