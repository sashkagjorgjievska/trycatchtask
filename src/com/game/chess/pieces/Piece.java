package com.game.chess.pieces;

import com.game.chess.Board;
import com.game.chess.Square;

/**
 * @author sashka gjorgjievska
 * @version 1.0
 *
 */
public interface Piece {
	
	/**
	 * Provides placement of each type of piece on <code>square</code>, given the <board>board</board> on which placement should occur.
	 * @param board the board (with size MxN) on which piece should be placed.
	 * @param square position of the piece on board.
	 * @return true if piece is successfully placed, false otherwise
	 */
	public boolean place(Board board, Square square) ;

}
