package com.game.chess;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.game.chess.pieces.Piece;

/**
 * Holds the solution set as a set of  of <code>Board</code> objects. 
 * Provides interface to this application.
 * 
 * @author sashka gjorgjievska
 * @version 1.0
 *
 */
public class Configuration {

	Set<Board> solutionSet = new HashSet<Board>();

	public Set<Board> getSolutionSet() {
		return solutionSet;
	}

	public void configure (Board aBoard, List<Piece> somePieces) {
		if (somePieces.size() == 0 ) {
			if (solutionSet.add(aBoard)) {
				aBoard.printBoard();
			}
			return;
		}
		Piece piece = somePieces.get(0);
		List<Square> freeSquares = aBoard.getFreeSquares();
		for (int j = 0; j < freeSquares.size(); j++) {
			Board nextBoard = new Board(aBoard);
			if (piece.place(nextBoard, freeSquares.get(j))) {
				configure(nextBoard, somePieces.subList(1, somePieces.size()));
			} else {
//				aBoard.resetBoard();
				return;
			}
		}
	}

}
