package com.game.chess;

import com.game.chess.pieces.Piece;
/**
 * Represents one square of the chess board, defined by <code>row</code> and <code>col</code>.
 * If <code>piece == null</code>, no piece is set on the square.
 * If <code>isProtected == false</code>, no piece from any other square protects this square.
 * 
 * @author sashka gjorgjievska
 * @version 1.0
 * 
 */
public class Square {

	private Piece piece = null;
	private boolean isProtected = false;
	private int row;
	private int col;

	public Square(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}
	public Square(Square square) {
		this.row = square.row;
		this.col = square.col;
		this.piece = square.piece;
		this.isProtected = square.isProtected;
	}
	public Piece getPiece() {
		return piece;
	}
	public void setPiece(Piece piece) {
		this.piece = piece;
	}
	public boolean isProtected() {
		return isProtected;
	}
	public void setProtected(boolean protect) {
		this.isProtected = protect;
	}
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public int getCol() {
		return col;
	}
	public void setCol(int col) {
		this.col = col;
	}
	public void printSquare() {
		String pieceStr = (piece != null) ? piece.getClass().getSimpleName() : " ";
		String protectStr = isProtected ? "Protected":"";
		System.out.println("{" + row + "," + col + "," + pieceStr  + "," + protectStr  + "}");

	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + col;
		result = prime * result + ((piece == null) ? 0 : 1);
		result = prime * result + row;
		result = prime * result + (isProtected ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Square)) {
			return false;
		}
		Square other = (Square) obj;
		if (col != other.col) {
			return false;
		}
		if (piece == null) {
			if (other.piece != null) {
				return false;
			}
		} else {
			Piece otherPiece = other.getPiece();
			if (otherPiece == null) {
				return false;
			} else {
				return piece.getClass().equals(otherPiece.getClass());
			}
		}
		if (row != other.row) {
			return false;
		}
		if (isProtected != other.isProtected) {
			return false;
		}
		return true;
	}



}
