package com.game.chess.utils;

import com.game.chess.Square;

public class ChessUtils {
	
	public static boolean checkAndSetSquare(Square squareToAttack) {
		if (squareToAttack.getPiece() == null) {
			squareToAttack.setProtected(true);
			return true;
		} 
		return false;
	}

}
