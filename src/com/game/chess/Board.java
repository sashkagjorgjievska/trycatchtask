package com.game.chess;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the chess board as a list of <code>Square</code> objects. 
 * Holds the current state of each possible board configuration while 
 * evaluating legal placements for the remaining pieces. 
 * 
 * @author sashka gjorgjievska
 * @version 1.0
 *
 */
public class Board {
	private int numOfRows;
	private int numOfCols;
	private List<Square> squares;


	public Board(int numOfRows, int numOfCols) {
		super();
		this.numOfRows = numOfRows;
		this.numOfCols = numOfCols;
		this.squares = new ArrayList<Square>();
		for (int i = 0; i < numOfRows; i++) {
			for (int j = 0; j < numOfCols; j++) {
				Square s = new Square(i, j);
				this.squares.add(s);
			}
		}
	}
	
	public Board(Board aBoard) {
		this.numOfRows = aBoard.numOfRows;
		this.numOfCols = aBoard.numOfCols;
		this.squares = new ArrayList<Square>();
		for (int i = 0; i < aBoard.numOfRows; i++) {
			for (int j = 0; j < aBoard.numOfCols; j++) {
				this.squares.add(new Square(aBoard.getSquares().get(i * numOfCols + j)));
			}
		}
	}
	public int getNumOfRows() {
		return numOfRows;
	}
	public void setNumOfRows(int numOfRows) {
		this.numOfRows = numOfRows;
	}
	public int getNumOfCols() {
		return numOfCols;
	}
	public void setNumOfCols(int numOfCols) {
		this.numOfCols = numOfCols;
	}
	public List<Square> getSquares() {
		return squares;
	}
	public void setSquares(List<Square> squares) {
		this.squares = squares;
	}
	public List<Square> getFreeSquares() {
		List<Square> freeSquares = new ArrayList<Square>();
		for (Square square : squares) {
			if (!square.isProtected() && square.getPiece() == null) {
				freeSquares.add(square);
			}
		}
		return freeSquares;
	}
	public void printBoard() {
		System.out.println("New Board:");
		System.out.println("{row,col,piece,protected}");
		System.out.println("=============================");
		for (Square square : squares) {
			square.printSquare();
		}
		System.out.println("=============================");

	}
	public void resetBoard() {
		for (Square square : squares) {
			square.setPiece(null);
			square.setProtected(false);
		}

	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + numOfCols;
		result = prime * result + numOfRows;
		result = prime * result + ((squares == null) ? 0 : squares.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Board)) {
			return false;
		}
		Board other = (Board) obj;
		if (numOfCols != other.numOfCols) {
			return false;
		}
		if (numOfRows != other.numOfRows) {
			return false;
		}
		if (squares == null) {
			if (other.squares != null) {
				return false;
			}
		} else if (!squares.equals(other.squares)) {
			return false;
		}
		return true;
	}
	
}
