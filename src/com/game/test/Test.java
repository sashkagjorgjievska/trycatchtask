package com.game.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.game.chess.Board;
import com.game.chess.Configuration;
import com.game.chess.pieces.Bishop;
import com.game.chess.pieces.King;
import com.game.chess.pieces.Knight;
import com.game.chess.pieces.Piece;

public class Test {

	public static void main(String[] args) {
		Configuration config = new Configuration();
		List<Piece> somePieces = new ArrayList<Piece>();
//		Queen q1 = new Queen();
//		Queen q2 = new Queen();
//		somePieces.add(q1);
//		somePieces.add(q2);
		
//		King k1 = new King();
//		King k2 = new King();
//		King k3 = new King();
//		somePieces.add(k1);
//		somePieces.add(k2);
//		somePieces.add(k3);
		
		Bishop b1 = new Bishop();
//		Bishop b2 = new Bishop();
//		Bishop b3 = new Bishop();
//		Bishop b4 = new Bishop();
		somePieces.add(b1);
//		somePieces.add(b2);
//		somePieces.add(b3);
//		somePieces.add(b4);
		
		Knight n1 = new Knight();
		Knight n2 = new Knight();
		Knight n3 = new Knight();
		Knight n4 = new Knight();
		somePieces.add(n1);
		somePieces.add(n2);
		somePieces.add(n3);
		somePieces.add(n4);
		
		config.configure(new Board(3,3), somePieces);
		Set<Board> solutions = config.getSolutionSet();
		if (solutions.isEmpty()) {
			System.out.println("This configuration is not feasible. Please try another one...");
		} else {
			System.out.println("There are " + solutions.size() + " different configurations.") ;
		}

	}

}
