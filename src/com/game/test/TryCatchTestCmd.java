package com.game.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.game.chess.Board;
import com.game.chess.Configuration;
import com.game.chess.pieces.Bishop;
import com.game.chess.pieces.EPieces;
import com.game.chess.pieces.King;
import com.game.chess.pieces.Knight;
import com.game.chess.pieces.Piece;
import com.game.chess.pieces.Queen;
import com.game.chess.pieces.Rook;

public class TryCatchTestCmd {

	public static void main(String[] args) {
		Configuration config = new Configuration();
		List<Piece> somePieces = new ArrayList<Piece>();
		Board aBoard = new Board(7,7);
		if (args.length > 2) {
			int rows = Integer.parseInt(args[0]);
			int cols = Integer.parseInt(args[1]);
			aBoard = new Board(rows, cols);
			Piece piece = null;
			boolean addPiece = true;
			for (int i = 2; i < args.length; i++) {
				switch (EPieces.valueOf(args[i])) {
				case Queen:
					piece = new Queen();
					break;
				case King:
					piece = new King();
					break;
				case Bishop:
					piece = new Bishop();
					break;
				case Rook:
					piece = new Rook();
					break;
				case Knight:
					piece = new Knight();
					break;
				default:
					addPiece = false;
					//shall never happen, if some other piece is entered, will get an exception
					System.out.println("Unsupported piece type: " + args[i]);
					break;
				}
				if (addPiece) {
					somePieces.add(piece);
				}
				addPiece = true;
			}
		} else {
			System.out.println("Invalid agruments. Calculating 7x7 board with 2 Kings, 2 Queens, 2 Bishops and 1 Knight.");
			Queen q1 = new Queen();
			Queen q2 = new Queen();
			somePieces.add(q1);
			somePieces.add(q2);
			
			King k1 = new King();
			King k2 = new King();
			somePieces.add(k1);
			somePieces.add(k2);
			
			Bishop b1 = new Bishop();
			Bishop b2 = new Bishop();
			somePieces.add(b1);
			somePieces.add(b2);
			
			Knight n1 = new Knight();
			somePieces.add(n1);
		}

		config.configure(aBoard, somePieces);
		Set<Board> solutions = config.getSolutionSet();
		if (solutions.isEmpty()) {
			System.out.println("This configuration is not feasible. Please try another one...");
		} else {
			System.out.println("There are " + solutions.size() + " different configurations.") ;
		}

	}

}