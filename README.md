# README #

This is a solution of the following problem: find all unique configurations of a set of normal chess pieces on a chess board with dimensions M×N where none of the pieces is in a position to take any of the others. Assume the colour of the piece does not matter, and that there are no pawns among the pieces.

The program takes as input:

      The dimensions of the board: M, N.

      The number of pieces of each type (King, Queen, Bishop, Rook and Knight) to try and place on the board.

As output, the program lists all the unique configurations to the console for which all of the pieces can be placed on the board without threatening each other.
Version 1.0

### How to run the program? ###

To run the program, please download chess.jar and run it using following command:

java -jar chess.jar [ M N Piece1 Piece2 .... PieceN]

where: 

      M=> number of rows

      N=> number of columns

      [Piece1..PieceN]=>{King|Queen|Rook|Knight|Bishop}

If command line arguments are not provided, it lists all possible combinations for a 7x7 board with 2 Kings, 2 Queens, 2 Bishops and 1 Knight.
